package br.com.mastertech.itau.imersivo.formas;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class App {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		
		System.out.println("Olá! bem vindo ao calculador de área 3 mil!");
		System.out.println("Basta informar a medida de cada lado que eu te digo a área :)");
		System.out.println("Vamos começar!");
		System.out.println("");
		System.out.println("Obs: digite -1 se quiser parar de cadastrar lados!");
		System.out.println("");
		
		List<Double> lados = new ArrayList<>();
		
		boolean deveAdicionarNovoLado = true;
		while(deveAdicionarNovoLado) {
			System.out.println("Informe o tamanho do lado " + (lados.size() + 1));
			
			double tamanhoLado = Double.parseDouble(scanner.nextLine());
			
			if(tamanhoLado <= 0) {
				deveAdicionarNovoLado = false;
			}else {
				lados.add(tamanhoLado);
			}
		}
		
		System.out.println("Lados cadastrados!");
		System.out.println("Agora vamos calcular a área...");
		
		
		if(lados.size() == 0) {
			System.out.println("Forma inválida!");
		}else if(lados.size() == 1) {
			Circulo circulo = new Circulo(lados);
			System.out.println(circulo.calcular());
		
		}else if(lados.size() == 2) {
			QuadradoRetangulo quadret = new QuadradoRetangulo(lados);
			System.out.println(quadret.calcular());
		}else if(lados.size() == 3) {
			Triangulo triangulo = new Triangulo(lados);
			System.out.println(triangulo.calcular());
				}else {
			System.out.println("Ops! Eu não conheço essa forma geometrica ¯\\_(⊙_ʖ⊙)_/¯");
		}
		
	}

}
