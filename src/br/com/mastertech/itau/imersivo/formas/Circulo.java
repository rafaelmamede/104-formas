package br.com.mastertech.itau.imersivo.formas;

import java.util.List;

public class Circulo extends Formas {

	public Circulo(List<Double> lados) {
		super(lados);
		// TODO Auto-generated constructor stub
	}

//	@Override
//	void calcularArea() {
//		System.out.println("Eu identifiquei um circulo!");
//		System.out.println("A área do circulo é " + (Math.PI * lados.get(0)));
//
//	}

	@Override
	public Double calcular() {
		Double area = (lados.get(0) * Math.PI);
		return area;
	}

}