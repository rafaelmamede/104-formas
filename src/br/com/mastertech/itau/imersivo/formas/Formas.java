package br.com.mastertech.itau.imersivo.formas;

import java.util.List;

public abstract class Formas {

	protected List<Double> lados;

//	abstract void calcularArea();

	public abstract Double calcular();

//	public List<Double> getLados() {
//		return lados;
//	}

//	public void setLados(List<Double> lados) {
//		this.lados = lados;
//	}

	public Formas(List<Double> lados) {
		super();
		this.lados = lados;
	}

}
