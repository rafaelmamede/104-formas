package br.com.mastertech.itau.imersivo.formas;

import java.util.List;

public class QuadradoRetangulo extends Formas {

	public QuadradoRetangulo(List<Double> lados) {
		super(lados);
		// TODO Auto-generated constructor stub
	}

//	@Override
//	void calcularArea() {
//		System.out.println("Eu identifiquei um quadrado/retangulo!");
//		System.out.println("A área do quadrado/retangulo é " + (lados.get(0) * lados.get(1)));
//	}

	@Override
	public Double calcular() {
		Double area = (lados.get(0) * lados.get(1));
		return area;
	}

}
